const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

exports.onMessageCreate = functions.database.ref('/com_list/{pushId}').onCreate((snapshot, context) => {
    var data = snapshot.val();
    var text = data.text;
    var email = data.user_email;
    return snapshot.ref.update({
        addedQQdata: `${text}QQ`,
        text: `${text}`,
        user_email: `${email}`
    });
})